import styled from "styled-components";
import TextField from "@material-ui/core/TextField";
import { useCounter } from "../utils/UseCounter";
import IconButton from "@material-ui/core/IconButton";
import ExpandLessRoundedIcon from "@material-ui/icons/ExpandLessRounded";
import Button from "@material-ui/core/Button";
import PauseIcon from "@material-ui/icons/Pause";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import CachedIcon from "@material-ui/icons/Cached";

export const Index = (props) => {
  const {
    start,
    counter,
    number,
    startCounter,
    changeNumber,
    incNumber,
    decNumber,
    reset,
    onMouseDown,
    onMouseUp,
  } = useCounter();

  return (
    <Container>
      <FirstChild>
        <h1>{counter === 0 ? "RUN" : counter}</h1>
      </FirstChild>
      <Child>
        <IconButton
          onMouseDown={() => onMouseDown("DEC")}
          onMouseUp={onMouseUp}
          onClick={decNumber}
        >
          <ExpandLessRoundedIcon
            style={{ transform: "rotate(180deg)", color: "whitesmoke" }}
            fontSize="large"
          />
        </IconButton>
        <TextField
          style={{ minWidth: "3rem", maxWidth: "5rem" }}
          value={number}
          onChange={changeNumber}
          id="outlined-number"
        />

        <IconButton
          onMouseDown={() => onMouseDown("INC")}
          onMouseUp={onMouseUp}
          onClick={incNumber}
        >
          <ExpandLessRoundedIcon
            style={{ color: "whitesmoke" }}
            fontSize="large"
          />
        </IconButton>
      </Child>
      <Child>
        <Button
          onClick={startCounter}
          variant="contained"
          color="primary"
          size="large"
          startIcon={start ? <PauseIcon /> : <PlayArrowIcon />}
        >
          {start ? "PAUSE" : "START"}
        </Button>

        <Button
          onClick={reset}
          variant="contained"
          color="primary"
          size="large"
          startIcon={<CachedIcon />}
        >
          {"RESET"}
        </Button>
      </Child>
    </Container>
  );
};

const FirstChild = styled.div`
  display: flex;
  width: 10rem;
  height: 10rem;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  background-color: #b9e9f0;
  margin: 0 auto;
  border-radius: 100px;
`;

const Child = styled.div`
  display: flex;
  width: 100;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  & > *:not(:last-child) {
    margin-right: 1rem;
  }
`;

const Container = styled.div`
  width: 70%;
  padding: 5rem 5rem;
  margin: 3rem auto;
  display: flex;
  flex-direction: column;
  background-color: #94a1a6;
  & > * {
    margin-bottom: 2rem;
  }
`;
