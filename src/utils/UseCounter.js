import { useEffect, useRef, useState } from "react";

const DEFAULT_NUMBER = 1000;
const DEFAULT_COUNTER = 0;

export function useCounter() {
  const [start, setStart] = useState(false);
  const [number, setnumber] = useState(DEFAULT_NUMBER);
  const [counter, setcounter] = useState(DEFAULT_COUNTER);

  const intervalRef = useRef(null);
  const mouseIntervalRef = useRef(null);

  const changeNumber = (event) => setnumber(Number(event.target.value));
  const incNumber = () => {
    setnumber(number + 1);
  };
  const decNumber = () => number > 0 && setnumber(number - 1);
  const reset = () => {
    clearInterval(intervalRef.current);
    setStart(false);
    setnumber(0);
    setcounter(0);
  };
  const startCounter = () => {
    number <= 0 ? alert("set a number ms") : setStart(!start);
  };
  const mainCounter = () => {
    if (start) {
      intervalRef.current = setInterval(() => {
        setcounter((counter) => counter + 1);
      }, number);
    }
  };
  const onMouseDown = (kind) =>
    (mouseIntervalRef.current = setInterval(() => {
      switch (kind) {
        case "DEC":
          setnumber((number) => (number > 0 ? number - 1 : 0));

          break;
        case "INC":
          setnumber((number) => number + 1);
          break;
        default:
          break;
      }
    }, 1));
  const onMouseUp = () => {
    clearInterval(mouseIntervalRef.current);
  };

  useEffect(() => {
    if (start) {
      mainCounter();
      return () => clearInterval(intervalRef.current);
    }
  }, [start, number]);

  useEffect(() => {
    if (number === 0) {
      clearInterval(intervalRef.current);
      clearInterval(mouseIntervalRef.current);
      reset();
    }
  }, [number]);

  return {
    start,
    counter,
    number,
    startCounter,
    changeNumber,
    incNumber,
    decNumber,
    reset,
    onMouseDown,
    onMouseUp,
  };
}
